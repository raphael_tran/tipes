"""Solving the traveling salesman problem using a genetic algorithm."""


from random import random, randint, shuffle
from numpy import sqrt
import matplotlib.pyplot as plt



### Problem related variables ###

n = 20

# Cities are generated randomly
lst_cities = [(10*random(), 10*random()) for k in range(n)]

lst_cities = [(1.7964953953425045, 8.84666344869724), (5.6363793528096195, 4.532608234760753), (3.045400509072792, 6.210679830355764), (7.073180009861208, 6.012699925536932), (0.3624251447112192, 1.5187442530064499), (5.5087997515303915, 4.160166935502803), (0.4559579596424512, 5.5589407052253526), (1.8276541867586538, 8.901319265130496), (7.568757166128841, 4.574564997806253), (8.72993069702238, 2.8637813466503914), (3.73632689180756, 5.336169880470423), (3.0051975965402877, 9.500924546140348), (6.922443176652035, 2.4882471404806914), (3.976276891127416, 2.1507938419509944), (7.682674494633095, 3.7364345175634783), (9.77566724362405, 9.726770168016932), (9.320143782176727, 8.572028770144732), (1.7469362575606762, 3.4633885652963716), (5.51917713806183, 4.437431582932751), (7.081283862896126, 4.497710736601471)]



### Showing ###

def show_cities():
	global lst_cities
	plt.axis([-1, 11, -1, 11])
	plt.axis('equal')
	for city in lst_cities:
		plt.plot([city[0]], [city[1]], 'bo', ms=7)

def show_tour(individual):
	global lst_cities
	show_cities()
	lst_x = [lst_cities[individual[k]][0] for k in range(n)]
	lst_y = [lst_cities[individual[k]][1] for k in range(n)]
	lst_x.append(lst_x[0])
	lst_y.append(lst_y[0])
	plt.plot(lst_x, lst_y)



### Miscellaneous functions ###

def distance(i, j):
	"""Return the distance between cities i and j."""
	return sqrt((lst_cities[j][0]- lst_cities[i][0])**2 + \
			(lst_cities[j][1]- lst_cities[i][1])**2)

def length(individual):
	"""Length of the tour defined by individual."""
	length = distance(individual[0], individual[-1])
	for k in range(1, n):
		length += distance(individual[k-1], individual[k])
	return length

def sort_pop(population, fitness):
	population.sort(key=lambda x: fitness(x))



### Initialisation ###

def generate_individual():
	indiv = [k for k in range(n)]
	shuffle(indiv)
	return indiv

def initialize(pop_size):
	pop = []
	best_fitness = 0
	for k in range(pop_size):
		pop.append(generate_individual())
		best_fitness = max(best_fitness, fitness(pop[-1]))
	return pop, best_fitness


### Evaluation ###

def fitness(individual):
	return 1 / length(individual)


### Parents selection

def selection1(population, fitness, sus=False):
	"""Fitness Proportional Selection.
	sus = Stochastic Universal Sampling"""
	pop_size = len(population)
	lst_a = []
	sum_fitness = 0
	for individual in population:
		sum_fitness += fitness(individual)
		lst_a.append(sum_fitness)

	mating_pool = []

	if sus:
		i = 0
		r = random() / pop_size
		while len(mating_pool) < pop_size:
			while r <= lst_a[i]/sum_fitness:
				mating_pool.append(population[i])
				r += 1/pop_size
			i += 1

	else:		# roulette wheel
		while len(mating_pool) < pop_size:
			r = sum_fitness * random()
			i = 0
			while lst_a[i] < r:
				i += 1
			mating_pool.append(population[i])

	return mating_pool


def selection2(population, fitness, sus=False):
	"""Ranking Selection."""
	pop_size = len(population)
	sort_pop(population, fitness)

	s = (pop_size * (pop_size + 1)) / 2
	lst_a = [1]
	for i in range(1, pop_size):
		lst_a.append(lst_a[-1] + i+1)

	mating_pool = []

	if sus:
		i = 0
		r = random() / pop_size
		while len(mating_pool) < pop_size:
			while r <= lst_a[i]/s:
				mating_pool.append(population[i])
				r += 1/pop_size
			i += 1

	else:		# if not sus:
		while len(mating_pool) < pop_size:
			r = random() / s
			i = 0
			while lst_a[i] < r:
				i += 1
			mating_pool.append(population[i])

	return mating_pool



### Crossover ###

def crossover1(parent1, parent2):
	"""Order crossover"""
	i = randint(0, n-2)
	j = randint(i+1, n)

	child1, child2 = [-1] * n, [-1] * n
	child1[i:j] = parent1[i:j]
	child2[i:j] = parent2[i:j]

	c1, c2 = 0, 0

	for k in range(n):
		x = parent2[k]
		y = parent1[k]

		if not (x in parent1[i:j]):
			child1[(c1+j)%n] = x
			c1 += 1

		if not (y in parent2[i:j]):
			child2[(c2+j)%n] = y
			c2 += 1

	return child1, child2



### Mutation ###

def no_mutation(individual):
	pass

def mutation1(individual):
	"""Swap mutation"""
	i = randint(0, n-2)
	j = randint(i+1, n-1)
	individual[i], individual[j] = individual[j], individual[i]

def mutation2(individual):
	"""Insert mutation"""
	i = randint(0, n-2)
	j = randint(i+1, n-1)
	while j > i+1:
		individual[j], individual[j-1] = individual[j-1], individual[j]
		j -= 1

def mutation3(individual):
	"""Scramble mutation
	!!! Does not work in place."""
	i = randint(0, n-2)
	j = randint(i+1, n)
	lst = individual[i:j]
	shuffle(lst)
	return individual[:i] + lst + individual[j:]

def mutation4(individual):
	"""Inversion mutation
	!!! Does not work in place."""
	i = randint(0, n-2)
	j = randint(i+1, n)
	lst = individual[i:j]
	return individual[:i] + lst[::-1] + individual[j:]



### Algorithm ###

def main(pop_size, max_gen, fitness, mutation, crossover, selection, sus,
		 survivors_selection=1):
	"""
	survivors_selection:
	0 -> Population totally replaced by childrens
	1 -> Next generation is selected like the parents
	2 -> Bests individuals from parents and children are selected
	"""

	lst_max_fitnesses = [None]

	population, lst_max_fitnesses[0] = initialize(pop_size)

	gen = 0

	lst_best = [population[0]]
	global_best = population[0]

	while gen < max_gen:
		# Parents selection
		mating_pool = selection(population, fitness, sus)

		# Generating children
		children = []
		best_individual = population[0]
		max_fitness = 0

		for i in range(0, pop_size, 2):
			child1, child2 = crossover(mating_pool[i], mating_pool[i + 1])

			mutation(child1)
			mutation(child2)

			children.append(child1)
			children.append(child2)

			fitness_child1 = fitness(child1)
			fitness_child2 = fitness(child2)

			if fitness_child1 > max_fitness:
				best_individual = list(child1)
				max_fitness = fitness_child1

			if fitness_child2 > max_fitness:
				best_individual = list(child2)
				max_fitness = fitness_child2


		lst_max_fitnesses.append(max_fitness)
		lst_best.append(best_individual)

		if fitness(best_individual) > fitness(global_best):
			global_best = list(best_individual)

		if survivors_selection == 0:
			# Replacing all the population.
			population = list(children)

		elif survivors_selection == 1:
			# Children selected the same way the parents were.
			population = selection(children + population, fitness, sus)
			population = population[:pop_size]

		elif survivors_selection == 2:
			# Best from parents and children are selected.
			# Bad complexity
			population.extend(children)
			population.sort(key=lambda x: fitness(x))
			population = population[pop_size:]

		gen += 1

		if gen%100 == 0:  print(gen)

	return lst_max_fitnesses, lst_best, global_best




### Execution ###

show_cities()
plt.show()


pop_size = 150
max_gen = 10000

lst_max_fitnesses, lst_best, best = \
		main(pop_size, max_gen, fitness, mutation2, crossover1, selection2, True, 2)


plt.figure()
lst_length = [1/f for f in lst_max_fitnesses]
plt.plot(range(max_gen + 1), lst_length)

plt.figure()
show_tour(best)
plt.show()

print("Best length:", length(best))


