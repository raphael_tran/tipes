"""KP Problem solved with dynamic and greedy programmation."""


wmax = 15		# Weight constraint (integer)
lst_values = [1, 2, 2, 10, 4]
lst_weights = [1, 1, 2, 4, 12]



def kp_dynamic(lst_values, lst_weights, wmax):
	"""Complexity:
		O(wmax) spatial
		O(wmax * n)"""
	n = len(lst_values)
	f = [(0, []) for i in range(wmax+1)]
	for k in range(n):
		for x in range(wmax, -1, -1):
			if lst_weights[k] <= x and lst_values[k] + f[x-lst_weights[k]][0] > f[x][0]:
				f[x] = lst_values[k] + f[x-lst_weights[k]][0], [k] + f[x-lst_weights[k]][1]
	return f[wmax]

def kp_greedy(lst_values, weight, wmax):
	"""OSEF en fait"""



print(kp_dynamic(lst_values, lst_weights, wmax))