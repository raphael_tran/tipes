"""KP Problem using a genetic algorithm."""


from random import random, randint
from numpy import exp
from matplotlib import pyplot



### Problem related variables ###

wmax = 15

lst_values = [1, 2, 2, 10, 4]
lst_weights = [1, 1, 2, 4, 12]

n = len(lst_values)

### Miscellaneous functions ###

def sort_pop(population, fitness):
	"""Simply select the m best individual."""
	population.sort(key=lambda x: fitness(x))



### Initialisation ###

def generate_individual():
	indiv = []
	for i in range(n):
		indiv.append(randint(0,1))
	return indiv

def initialize(pop_size):
	pop = []
	best_fitness = 0
	for k in range(pop_size):
		pop.append(generate_individual())
		best_fitness = max(best_fitness, fitness(pop[-1]))
	return pop, best_fitness



### Evaluation ###

def weight(individual):
	w_total = 0
	for i, x in enumerate(individual):
		w_total += x * lst_weights[i]
	return w_total

def is_valid(individual):
	return weight(individual) <= wmax

def value(individual):
	v_total = 0
	for i, x in enumerate(individual):
		v_total += x * lst_values[i]
	return v_total


def fitness1(individual):
	"""fitness1 == value ; only accept valid solutions."""
	if is_valid(individual):
		return value(individual)
	return 0

def fitness2(individual, exp_param=1):
	"""Enable invalid individualse to be selected with a penalty."""
	v = value(individual)
	w = weight(individual)
	if w <= wmax:
		return v
	return v * exp(-exp_param * (w - wmax))



### Parents selection

def selection1(population, fitness, sus=False):
	"""Fitness Proportional Selection
	sus = Stochastic Universal Sampling"""

	pop_size = len(population)
	lst_a = []
	sum_fitness = 0
	for individual in population:
		fit = fitness(individual)
		lst_a.append(sum_fitness + fit)
		sum_fitness += fit

	mating_pool = []

	if sus:
		i = 0
		r = sum_fitness * random() / pop_size
		while len(mating_pool) < pop_size:
			while r <= lst_a[i]:
				mating_pool.append(population[i])
				r += 1/pop_size
			i += 1

	else:		# roulette wheel
		while len(mating_pool) < pop_size:
			r = sum_fitness * random()
			i = 0
			while lst_a[i] < r:
				i += 1
			mating_pool.append(population[i])

	return mating_pool



def selection2(population, fitness, sus=False):
    """Ranking Selection"""
    pop_size = len(population)
    sort_pop(population, fitness)

    s = (pop_size * (pop_size + 1)) / 2
    lst_a = [1]

    for i in range(1, pop_size):
        lst_a.append(lst_a[-1] + i+1)

    mating_pool = []

    if sus:
        i = 0
        r = (random() / pop_size)
        while len(mating_pool) < pop_size:
            while r <= lst_a[i]/s:
                mating_pool.append(population[i])
                r += 1/pop_size
            i += 1

    else:        # if not sus:
        while len(mating_pool) < pop_size:
            r = random() / s
            i = 0
            while lst_a[i] < r:
                i += 1
            mating_pool.append(population[i])

    return mating_pool



### Crossover ###

def cross1(parent1, parent2):
	"""Classic crossover: cut in the middle."""
	i = randint(1, n-1)
	return parent1[:i] + parent2[i:], parent2[:i] + parent1[i:]

def cross2(parent1, parent2):
	"""Uniform crossover."""
	child1 = []
	child2 = []
	for i in range(n):
		if random() < 0.5:
			child1.append(parent1[i])
			child2.append(parent2[i])
		else:
			child1.append(parent2[i])
			child2.append(parent1[i])
	return child1, child2



### Mutation ###

def mutation1(individual, proba=1/n):
	"""Each bit has a chance to be flipped."""
	for i in range(n):
		if random() <= proba:
			individual[i] = 1 - individual[i]

### Algorithm ###

def main(pop_size, generations, fitness, mutation, crossover, selection, sus, selection_parents = 0):
	"""selection_parents:
	0 => Only children survives
	1 => Next generation is selected by selection function
	2 => Bests individuals from parents and children are selected
	"""

	max_fitnesses = [0]

	population, max_fitnesses[0] = initialize(pop_size)
	generation = 0

	best_individual = list(population[0])

	while generation < generations:
		#Parents selection
		mating_pool = selection(population, fitness, sus)

		#Children generating
		children = []
		max_fitness = 0

		for i in range(0, len(mating_pool), 2):
			child1, child2 = crossover(mating_pool[i], mating_pool[i + 1])

			mutation(child1)
			mutation(child2)

			children.append(child1)
			children.append(child2)

			fitness_child1 = fitness(child1)
			fitness_child2 = fitness(child2)

			if(fitness_child1 > max(max_fitnesses)):
				best_individual = list(child1)

			if(fitness_child2 > max(max_fitnesses)):
				best_individual = list(child2)

			max_fitness = max(max_fitness, fitness_child1, fitness_child2)


		max_fitnesses.append(max_fitness)

		if selection_parents == 1:
			population = selection(children + population, fitness, sus)
			population = population[0:len(population)//2]

		elif selection_parents == 2:
			#Bests from parents and children are selected
			population += list(children)
			population.sort(key=lambda x: fitness(x))
			population = population[len(population)//2:]

		else:
			#Replacing all the population
			population = list(children)

		generation += 1

	return max_fitnesses, best_individual


generations = 1000
max_fitnesses, best_individual = main(10, generations, fitness1, mutation1, cross2, selection2, False, 2)
pyplot.plot(range(generations +1), max_fitnesses)