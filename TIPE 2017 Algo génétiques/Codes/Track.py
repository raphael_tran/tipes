"""Implementation of the object characterizing the track (using lists)"""

from random import randint



class Track:
	"""Object which represents the track.

	width	-> width of the track
	length	-> length before finishing line (let 0 for a blank object)
	aperture	-> number of aperture tiles in an obstacle line (default 2)
	gap		-> number of line between two obstacles (default 3)
	start	-> number of empty lines before the obstacles (default 0)
	"""



	symbols = [' ', '■']
	border = '|'
	sparse = ' '


	def __init__(self, width, length=0, aperture=2, gap=3, start=0):
		"""Constructor of the Track class"""

		# Checking exceptions
		if not isinstance(width, int) or width <= 0:
			raise ValueError("The track width must be a strictly positive integer.")
		if not isinstance(length, int) or length < 0:
			raise ValueError("The track length must be a positive integer.")
		if not isinstance(gap, int) or gap <= 0:
			raise ValueError("The gap between obstacles must be a strictly positive integer.")
		if not isinstance(start, int) or start < 0:
			raise ValueError("The starting length must be a positive integer.")

		self.width = width

		# Building the track
		c = start + gap		# Counter
		self.track = [[0] * width] * min(length, c)		# Starting area
		while c <= length:

			# Obstacle line
			obstacle_index = randint(0, width - aperture)
			obstacle_line = []
			for j in range(obstacle_index):
				obstacle_line.append(1)
			for j in range(aperture):
				obstacle_line.append(0)
			for j in range(width - aperture - obstacle_index):
				obstacle_line.append(1)
			self.track.append(obstacle_line)
			c += 1

			# Gap between obstacles
			for i in range(gap):
				self.track.append([0] * width)
			c += gap



	def __repr__(self):
		"""Print the appearance of the track."""
		output = str()
		for line in self.track[::-1]:
			line_str = []
			for j in line:		# Converting numbers to symbol
				line_str.append(self.symbols[j])
			output += self.border + self.sparse.join(line_str) + self.border + '\n'
		return output


	def __getitem__(self, index):
		"""Return the index line as a list which contains integers."""
		try:
			return self.track[index]
		except IndexError:
			raise IndexError("Invalid index")

	def __setitem__(self, index, value):
		"""Replace a line by another one."""
		if not index in range(len(self.track)):
			raise IndexError("Invalid index")
		if not isinstance(value, list) or len(value) != self.width:
			raise ValueError("You must enter a list of length {}.".format(self.width))
		self.track[index] = value

	def __add__(self, to_append):
		"""Overloading the + operator

		Can be added:
			list -> it extend the from one line
			another Track object -> they are added one after another
		"""
		pass
		#---------------------


	def extend(self, steps, aperture, aperture_index="auto"):
		"""Add an obstacle several steps further with a specific aperture."""
		self.track.extend([[0] * self.width] * steps)		# Adding blank lines

		# --------------------Obstacle
		pass





















