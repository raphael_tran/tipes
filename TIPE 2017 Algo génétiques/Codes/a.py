from random import random,randint
from math import pi,cos,sin,sqrt,floor
from numpy import linspace
import matplotlib.pyplot as plt
def rs(): return (-1)**randint(0,1)
g=9.81
xG=5
z0=1
v0=5
alpha=pi/4
d_z0=[0,None]
d_v0=[0,None]
d_alpha=[-pi/2,pi/2]
c_z0=1
c_v0=2
c_alpha=pi/6
e=0.001
ng=3
max_gen=999
TEMP=1
def temp(d, slope=1): return slope*d
initial_color='00BF96'
final_color='E50031'
wx,wz=1.1,1.1
def xP(z0,v0,alpha):
	s=sin(alpha)
	return v0**2*cos(alpha)*(s+sqrt(s**2+2*g*z0/v0**2))/g
def h(xG,z0,v0,alpha): return abs(xG-xP(z0,v0,alpha))
def check(q,a,b): return (a is None or a<q) and (b is None or q<b)
def show(xG,xmax,z0,v0,alpha,n=1000,color='b'):
	c=cos(alpha)
	l_x=linspace(0,xmax,n+1)
	l_z=[]
	for x in l_x:
		l_z.append(z0-g/(2*(v0*c)**2)*x**2+sin(alpha)/c*x)
	plt.plot(l_x,l_z,color=color)
def ku(l_best):
	l_index=[0]
	last_b=l_best[0]
	for i,b in enumerate(l_best[1:]):
		if b!=last_b:
			l_index.append(i+1)
			last_b=b
	return len(l_index),l_index
n_gen=1
characteristic_quantities=[c_z0,c_v0,c_alpha]
domains=[d_z0,d_v0,d_alpha]
best=[z0,v0,alpha]
l_best=[best]
dist=h(xG,*best)
while n_gen<=max_gen and dist>e:
	n_gen+=1
	l_gen=[]
	for i in range(ng):
		unit=[]
		for k,q in enumerate(best):
			ok=False
			while not(ok):
				new_q=q+rs()*random()*temp(dist)*characteristic_quantities[k]
				ok=check(new_q,*domains[k])
			unit.append(new_q)
		l_gen.append(unit)
	for unit in l_gen:
		dist_unit=h(xG,*unit)
		if dist_unit<dist:
			dist=dist_unit
			best=list(unit)
	l_best.append(best)
n_evolutions,l_index=ku(l_best)
print("\nResults ({} generations with {} concrete evolutions):".format(n_gen,n_evolutions))
print("z0 = "+str(best[0]))
print("v0 = "+str(best[1]))
print("alpha = "+str(best[2]))
print("\nDistance: "+str(dist))
l_xmax=[]
l_zmax=[]
for b in l_best:
	l_xmax.append(max(xG, xP(*b)))
	l_zmax.append(b[0]+.5*(b[1]*sin(b[2]))**2/g)
xmax=wx*max(l_xmax)
zmax=wz*max(l_zmax)
plt.xlabel("x")
plt.ylabel("N")
plt.axis([0,xmax,0,zmax])
plt.plot([xG],[0],'ro',ms=13)
RGB_init=[int(initial_color[2*k:2*k+2],16) for k in range(3)]
RGB_final=[int(final_color[2*k:2*k+2],16) for k in range(3)]
RGB=[]
for j,i in enumerate(l_index):
	color='#'
	for k in range(3):
		component=RGB_init[k]+(RGB_final[k]-RGB_init[k])*j/(n_evolutions-1)
		component=hex(floor(component))[2:]
		if len(component)==1: component='0'+component
		color+=component
	show(xG,xmax,*l_best[i],color=color)