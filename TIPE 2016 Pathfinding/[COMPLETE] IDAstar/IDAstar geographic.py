# -*- coding: utf-8 -*-

try:
	from boobs import boobs
except ModuleNotFoundError:
	pass


### Data ###
# !!! Those data have to be defined with those names !!!

start = [1, 2]
goal = [5, 0]
obstacles = [[1,1],[2,1],[2,2],[2,3]]




try:		# Reseting the program
	del(path)
except NameError:
	pass


# Graphic representation

def show(xmin="auto", xmax="auto", ymin="auto", ymax="auto"):
	"""Show the adapted grid to the current situation and return the bounds used."""

	p = []
	try:		# If the path is not calculated yet
		p = list(path)
	except:
		p = [start, goal]

	# First, finding most adequat bounds
	X = [A[0] for A in p+obstacles]
	Y = [A[1] for A in p+obstacles]
	if xmin == "auto":
		xmin = min(X) - 1
	if xmax == "auto":
		xmax = max(X) + 1
	if ymin == "auto":
		ymin = min(Y) - 1
	if ymax == "auto":
		ymax = max(Y) + 1

	# Then, showing the grid
	X, Y = xmax - xmin + 1, ymax - ymin + 1
	grid = [['·' for i in range(X)] for j in range(Y)]	# Generating an empty grid

	grid[start[1]-ymin][start[0]-xmin] = 'S'
	grid[goal[1]-ymin][goal[0]-xmin] = 'G'
	for obs in obstacles :
		grid[obs[1]-ymin][obs[0]-xmin] = '■'

	p.pop(0)		# Removing the points which correspond to start and goal
	p.pop(-1)
	for A in p :		# Tracing the path
		grid[A[1]-ymin][A[0]-xmin] = 'x'

	for j in range(Y):
		print(" ".join(grid[Y-j-1]))		# Showing grid in the correct way

	return xmin, xmax, ymin, ymax



# Required functions to IDA*

def norm(A,B):
	"""Return the Manhattan distance between A and B.
	It is the current heuristic function.
	"""
	return abs(B[1]-A[1]) + abs(B[0]-A[0])

def adjacent(A):
	"""Return the list of the adjacent tiles to A considering the obstacle tiles."""
	x, y = A[0], A[1]
	V = list()
	for B in [[x-1,y], [x+1,y], [x,y-1], [x,y+1]]:
		if not(B in obstacles) :
			V.append(B)
	return V



# IDA*

def search(tile, g, lim):
	f = g + norm(tile, goal)		# f is the evaluation function which provide an approximation of the path length from this tile.

	if f > lim:
		return f
	if tile == goal:
		return [tile]

	mini = 4 * lim		# It's a choice about the moment the algorithm gives up.
	for v in adjacent(tile):
		t = search(v, g+1, lim)
		if isinstance(t, list):
			t.append(tile)
			return t
		if t < mini:
			mini = t
	return mini

def ida_star():
	"""Return the path from start to goal in a list of tiles."""
	lim = norm(start, goal)
	while 1:
		t = search(start, 0, lim)
		if isinstance(t, list) :
			t.reverse()
			return t
		if t == 4 * lim:
			return "Goal is not accessible or too far"
		lim = t



# Use of the algorithm

print("\n")
print("Situation:\n")
xmin, xmax, ymin, ymax = show()
print("\n\nSolution:\n")
path = ida_star()		# The path must be named "path".
show(xmin, xmax, ymin, ymax)


#boobs()


