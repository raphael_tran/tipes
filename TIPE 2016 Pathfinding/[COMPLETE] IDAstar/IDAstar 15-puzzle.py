# -*- coding: utf-8 -*-


"""
Positions are coded in a list.

Then:
[6, 7, 14, 4, 5, 0, 3, 11, 2, 8, 10, 13, 9, 15, 1, 12] correspond to

    6  7 14  4
    5  0  3 11
    2  8 10 13
    9 15  1 12
"""


solved = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0]



# Functions to modify 15-puzzle

def move(taquin, direction):
	"""Return the position got when moving the black tile to direction.
	0 -> UP ; 1 -> RIGHT ; 2 -> DOWN ; 3 -> LEFT
	DOES NOT CHANGE THE taquin VARIABLE  &  DOES NOT DETECT ANY ERROR.
	"""

	dic = {0:-4, 1:+1, 2:+4, 3:-1}
	pos0 = taquin.index(0)
	taquin2 = list(taquin)
	taquin2[pos0], taquin2[pos0+dic[direction]] = taquin[pos0+dic[direction]], taquin[pos0]
	return taquin2



# Graphic representation

def show(taquin):
	"""Show the 15-puzzle."""
	str_taquin = [str(x) for x in taquin]
	print('')
	for i in range(4):
		print(' '.join(str_taquin[4 * i : 4 * i + 4]))



# Required functions to IDA*

def heuristic1(taquin):
	"""Somme des distances de Manhattan de chaque tile à son emplacement final."""
	h = 0
	for i, elmt in enumerate(taquin):
		if elmt != 0:
			dx = abs((elmt - 1)%4 - i%4)
			dy = abs((elmt - 1)//4 - i//4)
			h += dx + dy
	return h

def adjacent(taquin):
	"""Return the list of the adjacent positions (un peu moche mais c'est pas grave)."""
	pos0 = taquin.index(0)
	x0, y0 = pos0%4, pos0//4

	directions = list()
	if y0 != 0:
		directions.append(0)
	if x0 != 3:
		directions.append(1)
	if y0 != 3:
		directions.append(2)
	if x0 != 0:
		directions.append(3)

	adj = list()
	for i in directions:
		adj.append(move(taquin, i))
	return adj



# IDA*

def search(taquin, g, lim):
	f = g + heuristic1(taquin)

	if f > lim:
		return f
	if taquin == solved:
		return [taquin]

	mini = 81
	for v in adjacent(taquin):
		t = search(v, g+1, lim)
		if isinstance(t, list):
			t.append(taquin)
			return t
		if t < mini:
			mini = t
	return mini


def ida_star(taquin):
	"""Return the list of positions to solve the 15-puzzle from the initial position."""
	lim = heuristic1(taquin)
	while 1:
		print(lim)
		t = search(taquin, 0, lim)
		if isinstance(t, list):
			return t
		if t ==  81:
			return "Error"
		lim = t



# Use of the algorithm

test = [6, 14, 12, 1, 5, 0, 4, 9, 7, 11, 8, 3, 13, 10, 2, 15]

solution = ida_star(test)

solution.reverse()

c = 0
for x in solution:
	show(x)
	print("Nombre de coups : {}".format(c))
	c += 1


