# -*- coding: utf-8 -*-

try:
    del(path)
except:
    True   

# Various functions






# Graphic representation

def show(xmin="auto", xmax="auto", ymin="auto", ymax="auto"):
    """Show the adapted grid to the current situation and return the bounds used"""
    
    p = []
    try:            # If the path is not calculated yet
        for elmt in path:       #Copying path
            p.append(elmt)
    except:
        p = [start, goal]
    
    # First, finding most adequat bounds
    X = [A[0] for A in p+obstacles]
    Y = [A[1] for A in p+obstacles]
    if xmin == "auto" :
        xmin = min(X)-1
    if xmax == "auto" :
        xmax = max(X)+1
    if ymin == "auto" :
        ymin = min(Y)-1
    if ymax == "auto" :
        ymax = max(Y)+1
        
    # Then, showing the grid    
    X, Y = xmax - xmin + 1, ymax - ymin + 1
    grid = [['·' for i in range(X)] for j in range(Y)]    # Generating an empty grid
    
    grid[start[1]-ymin][start[0]-xmin] = 'S'
    grid[goal[1]-ymin][goal[0]-xmin] = 'G'
    for obs in obstacles :
        grid[obs[1]-ymin][obs[0]-xmin] = '■'
    
    p.pop(0)        # Removing the points which correspond to start and goal
    p.pop(-1)
    for A in p :        # Tracing the path 
        grid[A[1]-ymin][A[0]-xmin] = 'x'
    
    for j in range(Y) :
        print(" ".join(grid[Y-j-1]))      # Showing grid in the correct way
    
    return xmin, xmax, ymin, ymax

    
    
# Required functions to A*

def norm(A,B):
    """Return the Manhattan distance between A and B
    It is the current heuristic function"""
    return abs(B[1]-A[1]) + abs(B[0]-A[0])

def adjacent(A):
    """Return the list of the adjacent tiles to A considering the obstacle tiles"""
    x, y = A[0], A[1]
    V = list()
    for B in [ [x-1,y], [x+1,y], [x,y-1], [x,y+1] ]:
        if not(B in obstacles) :
            V.append(B)
    return V

def add_sorted(elmt, queue):
    f = elmt[2]
    
    i = 0
    while f <= queue[i][2] :
        i += 1
    queue



# !!! Those data have to be defined with those names !!!

start = [1, 2]
goal = [5,0]
obstacles = [[1,1],[2,1],[2,2],[2,3]]



# A*

def astar(start, goal): 
    nstart = [start, 0, norm(start, goal)]
    
    closedSet = list()
    openSet = [nstart]
    #cameFrom = 
    
    while len(openSet) != 0 :
        current = openSet.pop()
        if current[0] == goal :
            return current[1]
            return reconstruct_path(cameFrom, current)
        
        closedSet.append(current)
        for v in adjacent(current) :
            nv = [v, current[1], norm(v, goal)]
            if nv est pas déjà dans la closedlist ou la openlist en moins cher:
                nv[1] = u[1] + 1
                nv[2] = v[1] + norm(v,goal)
                add_sorted(nv, openSet)
    return "Failure"


def reconstruct_path(cameFrom, current):
    path = [current]
    while current in cameFrom.Keys :
        current = cameFrom[current]
        path.append(current)
    return path



print("\n")
print("Situation:\n")
xmin, xmax, ymin, ymax = show()
print("\n\nSolution:\n")
print(astar(start, goal))
print("Done")