# -*- coding: utf-8 -*-

from math import factorial
from random import randint 


    
position_de_reference = "12345678*8"
# Codage du taquin initial, la dernière valeur correspond à l'emplacement du zéro (tile vide)

def affiche(taquin):
    """Affiche le taquin dans sa position actuelle"""
    d = ' ~~~~~~~'
    print(d)
    print('|',taquin[0],taquin[1],taquin[2],'|')
    print('|',taquin[3],taquin[4],taquin[5],'|')
    print('|',taquin[6],taquin[7],taquin[8],'|')
    print(d)
        
def initialise():
    """Génère un taquin aléatoire (pas forcément soluble)"""    
    position_du_zero = 8
    positions = [str(k) for k in range(1,9)] + ['*']
    res = list()
    for i in range(8):
        a = randint(0,8-i)
        car = positions.pop(a)
        if car=='*' :
            position_du_zero=i
        res.append(car)   
    return ''.join(res+positions) + str(position_du_zero)

normalisateur = {'1':0,'2':1,'3':2,'4':3,'5':4,'6':5,'7':6,'8':7,'*':8}

def signature(taquin):
    """Renvoie la signature du taquin"""
    permutation=[normalisateur[car] for car in taquin[:-1]]
    pisteur=[True]*9
    totalisateur=0
    for indice in range(9):
        indice_temp=indice
        compteur=0
        while pisteur[indice_temp]:
            pisteur[indice_temp]=False
            indice_temp=permutation[indice_temp]
            compteur+=1
        if compteur>0:    
            totalisateur=totalisateur+compteur-1
    return (totalisateur+int(taquin[9]))%2           
            

affiche(position_de_reference)
print(signature(position_de_reference))
total = factorial(9)//2
print("Nombres de possibilités : ", total)
taquin = initialise()
affiche(taquin)
print(signature(taquin))

mouvements =\
[[1,3],[0,2,4],[1,5],
[0,6,4],[1,3,5,7],[2,4,8],
[3,7],[4,6,8],[5,7]]

def bouge_taquin(taquin,x,y):
    """Fait passer le zéro ('*') du tile x au tile y"""
    newtaquin=''
    for i in range(9):
        if i==y:
            newtaquin += taquin[x]
        elif i==x:
            newtaquin += taquin[y]
        else:
            newtaquin += taquin[i]
    return newtaquin + str(y)

print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\n\n\n")
test1 = bouge_taquin(position_de_reference,8,7) 
test2 = bouge_taquin(test1,7,4)
test3 = bouge_taquin(test2,4,1)
print(test1)
print(test2)
print(test3)
affiche(test1)
affiche(test2)
affiche(test3)



def atteindre(n):
    res=[[position_de_reference]  ]
    for i in range(1,n):
        if res[i-1]==set():
            return res
        res.append(set())
        for position in res[i-1]:
            position_du_zero=int(position[9])
            for mouv in mouvements[position_du_zero]:
                new_taquin=bouge_taquin(position, position_du_zero,mouv)
                nouveau=True
                for j in range(i):
                    if new_taquin in res[j]:
                        nouveau=False
                if nouveau:
                    res[i].add(new_taquin)
    return res

def aff(sol):
    cumul=0
    for i,liste in enumerate(sol):
        cumul+=len(liste)
        print("Profondeur : {0:2d}  ---> {1:6d} solutions.    Cumul : {2:6d}     {3:2.2f}%  ".format(i,len(liste),cumul,100*cumul/total))

def aff1(sol)  :
    for i,liste in enumerate(sol):
        print("Profondeur : ",i)
        for a in liste:
            print(a[:-1])


sol=atteindre(40)
aff(sol)


def distance(taquin1,taquin2):
    res=9
    for i in range(9):
        if taquin1[i] == taquin2[i]:
            res -=1
    return res

def resoudre(taquin,mouvt=0):
    affiche(taquin)
    if signature(taquin)==1:
        print("Taquin insoluble")
    for i,liste in enumerate(sol):
        if taquin in liste :
            if i==0:
                return None
            for newtaquin in sol[i-1]  :
                if distance(taquin,newtaquin)==2:
                    print("mouvement",mouvt+1)
                    resoudre(newtaquin,mouvt+1)
                    return None

#taquintest =initialise()
resoudre(taquintest)